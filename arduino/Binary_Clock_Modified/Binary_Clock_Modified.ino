#include "Tlc5940.h"

/*
An open-source binary clock for Arduino. 
Based on the code from by Rob Faludi (http://www.faludi.com)
Code under (cc) by Daniel Spillere Andrade, www.danielandrade.net
http://creativecommons.org/license/cc-gpl
*/

int second=50, minute=9, hour=0; //start the time on 00:00:00
int munit,hunit,valm=0,valh=0,ledstats,i;

void setup() { 
  Serial.begin(9600);

  Tlc.init();
}

void loop() {

static unsigned long lastTick = 0; // set up a local variable to hold the last time we moved forward one second
// (static variables are initialized once and keep their values between function calls)
// move forward one second every 1000 milliseconds

Serial.println(second);

if (millis() - lastTick >= 1000) {
  lastTick = millis();
  second++;

}

// move forward one minute every 60 seconds
  if (second >= 60) {
  minute++;
  second = 0; // reset seconds to zero
}

// move forward one hour every 60 minutes
if (minute >=60) {
  hour++;
  minute = 0; // reset minutes to zero
}

if (hour >=24) {
  hour  = 0;
  minute  = 0; // reset minutes to zero
}
  munit = minute%10; //sets the variable munit and hunit for the unit digits
  hunit = hour%10;

  ledstats = digitalRead(0);  // read input value, for setting leds off, but keeping count
  if (ledstats == LOW) {
    
  for(i=1;i<=13;i++){
    
    enableLED(i, LOW);}

  } else  {

  //minutes units
  if(munit == 1 || munit == 3 || munit == 5 || munit == 7 || munit == 9) {  enableLED(11, HIGH);} else {  enableLED(11,LOW);}
  if(munit == 2 || munit == 3 || munit == 6 || munit == 7) {enableLED(12, HIGH);} else {enableLED(12,LOW);}
  if(munit == 4 || munit == 5 || munit == 6 || munit == 7) {enableLED(13, HIGH);} else {enableLED(13,LOW);}
  if(munit == 8 || munit == 9) {enableLED(14, HIGH);} else {enableLED(14,LOW);}

  //minutes 
  if((minute >= 10 && minute < 20) || (minute >= 30 && minute < 40) || (minute >= 50 && minute < 60))  {enableLED(7, HIGH);} else {enableLED(7,LOW);}
  if(minute >= 20 && minute < 40)  {enableLED(6, HIGH);} else {enableLED(6,LOW);}
  if(minute >= 40 && minute < 60) {enableLED(5, HIGH);} else {enableLED(5,LOW);}

  //hour units
  if(hunit == 1 || hunit == 3 || hunit == 5 || hunit == 7 || hunit == 9) {enableLED(8, HIGH);} else {enableLED(8,LOW);}
  if(hunit == 2 || hunit == 3 || hunit == 6 || hunit == 7) {enableLED(9, HIGH);} else {enableLED(9,LOW);}
  if(hunit == 4 || hunit == 5 || hunit == 6 || hunit == 7) {enableLED(10, HIGH);} else {enableLED(10,LOW);}
  if(hunit == 8 || hunit == 9) {enableLED(11, HIGH);} else {enableLED(11,LOW);}

  //hour
  if(hour >= 10 && hour < 20)  {enableLED(1, HIGH);} else {enableLED(1,LOW);}
  //if(hour >= 20 && hour < 24)  {enableLED(13, HIGH);} else {enableLED(13,LOW);} TODO: fix for 12 hour time

  }

  valm = analogRead(0);    // add one minute when pressed
   if(valm<800) {
    //Serial.println("Added One Minute");
   /*minute++;
   second = 0;
   delay(250);*/
  }
  
  valh = analogRead(5);    // add one hour when pressed
   if(valh<800) {
    //Serial.println("Added one hour");
   /*hour++;
   second = 0;
   delay(250);*/
  }
}

void enableLED(int channel, int value){
  if(value)
    Tlc.set(channel, 1000);
  else
    Tlc.set(channel, 0);

  Tlc.update();
}

