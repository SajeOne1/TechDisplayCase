
int pins[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
int state = LOW;

// the setup function runs once when you press reset or power the board
void setup() {
  for(int i = 0; i < sizeof(pins) / sizeof(int); i++){
    pinMode(pins[i], OUTPUT);
  }
}

// the loop function runs over and over again forever
void loop() {
  state = !state;
  for(int i = 0; i < sizeof(pins) / sizeof(int); i++){
    digitalWrite(pins[i], state);
    delay(200);
  }

  delay(500);
}
