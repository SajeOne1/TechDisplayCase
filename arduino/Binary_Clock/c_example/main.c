#include <stdio.h> // STDOUT for debug
#include <time.h> // execution time( clock() )

// Offset time
int offset = 123; // Milli offset
// Active LED pins
int led[] = {0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14}; // LEDs in use

// Function declarations
void loop();
int* whatLEDS();
int getCurTime();
int hours(int millis);
int minutes(int millis);
int seconds(int millis);

int startTime;

int main(int argc, char* argv[]){
    startTime = clock();

    while(1){
	loop();
    }

    return 0;
}

// Arduino like loop function
void loop()
{
    int time = getCurTime();
    printf("%d\n", time);
}

// returns array of LEDS to turn on
int* whatLEDS(){
    return 0x0; // Null pointer temporarily
}

// Get current time in milliseconds
int getCurTime(){
    int time_spent;
    int curTime = clock();
    time_spent = (double)(curTime - startTime) / CLOCKS_PER_SEC;
    return time_spent;
}


// Convert milliseconds to hours
int hours(int seconds){
    seconds /= 60; // Minutes
    seconds /= 60; // Hours;

    return seconds;
}

// Convert milliseconds to minutes
int minutes(int seconds){
    seconds /= 60;

    return seconds;
}
