<!DOCTYPE html>

<html>
    <head> <title>GWSS Tech Display Case</title>
        <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    </head>
    <body>
        <div id="header">
            <img src="images/chargers.png" alt="GWSS Chargers"/>
            <h1>Welcome to the GWSS Tech Case!</h1>
        </div><br style="clear:both;"/>

        <div id="content">
            <p>Here you can control the projects currently in the case. Click one of the buttons below to begin controlling a project</p>

            <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
		    <input name="project" type="submit" value="Useless Box"/>
		    <input name="project" type="submit" value="Robot Arm"/>
		    <input name="project" type="submit" value="Elevator"/>
	    </form>

        </div>
    </body>
</html>

<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

require 'pyInterface.php';

if(isset($_POST['project']) && !empty($_POST['project'])){	
	$project = $_POST['project'];
	
	switch($project){
		case "Useless Box":
			activateProject(9);
			break;
		case "Robot Arm":
			activateProject(10);
			break;			
		case "Elevator":
			header("Location: http://play.me/elevator.php");
			break;
	}
}
?>
