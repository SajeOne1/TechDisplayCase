<!DOCTYPE html>

<html>
    <head>
	<title>Elevator - GWSS Tech Display Case</title>
        <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    </head>
    <body>
        <div id="header">
            <img src="images/chargers.png" alt="GWSS Chargers"/>
            <h1>Tech Display Case Elevator Control</h1>
        </div><br style="clear:both;"/>

        <div id="content">
            <p>Here you can control the elevator in the project case. Select a floor and the elevator will travel to it!</p>

            <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
		    <input name="project" type="submit" value="Floor 4"/>
		    <input name="project" type="submit" value="Floor 3"/>
		    <input name="project" type="submit" value="Floor 2"/>
		    <input name="project" type="submit" value="Floor 1"/>
		    <input name="project" type="submit" value="Go Back"/>
	    </form>

        </div>
    </body>
</html>

<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

require 'pyInterface.php';

if(isset($_POST['project']) && !empty($_POST['project'])){	
	$project = $_POST['project'];
	
	switch($project){
		case "Floor 4":
			activateProject(22);
			break;
		case "Floor 3":
			activateProject(27);
			break;			
		case "Floor 2":
			activateProject(17);
			break;
		case "Floor 1":
			activateProject(4);
			break;
		case "Go Back":
			header("Location: http://play.me/");
			break;
	}
}
?>
